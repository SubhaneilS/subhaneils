package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class MethodRepository {
	static WebDriver d1;
	
	public static void browserLaunch(){
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Testing\\Tools\\chromedriver.exe");
		d1 = new ChromeDriver();
		d1.manage().window().maximize();
				
	}
	
	public static void appLaunch() throws InterruptedException{
		Thread.sleep(3000);
		d1.get("http://newtours.demoaut.com/");
		
	}
	
	// identifying element by name.
	/*public static void validLogin() throws InterruptedException{
		WebElement uname= d1.findElement(By.name("userName"));
		uname.sendKeys("dasd");
		Thread.sleep(3000);
		WebElement passw = d1.findElement(By.name("password"));
		passw.sendKeys("dasd");
		Thread.sleep(3000);
		WebElement log = d1.findElement(By.name("login"));
		log.click();
		
	}*/
	
	public static void verifyValidLogin() throws InterruptedException{
		String expectedtitle="Find a Flight: Mercury Tours:";
		String actualobject=d1.getTitle();
		if (expectedtitle.equalsIgnoreCase(actualobject)){
			System.out.println("Valid Login");
			
			
		}else{
			System.out.println("Invalid Login");
		}
		
	}
	
	// identifying element by xpath.
	public static void validLogin() throws InterruptedException, AWTException{
		WebElement uname= d1.findElement(By.name("userName"));
		uname.sendKeys("dasd");
		Thread.sleep(3000);
		WebElement passw = d1.findElement(By.name("password"));
		passw.sendKeys("dasd");
		Thread.sleep(3000);
		//WebElement log = d1.findElement(By.xpath("//input[@value='Login']"));
		//log.click();
		Robot r1=new Robot();
		r1.keyPress(KeyEvent.VK_TAB);
		r1.keyRelease(KeyEvent.VK_TAB);
		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
	public static void departingFromSelection() throws InterruptedException{
		Select s1=new Select (d1.findElement(By.name("fromPort")));
		s1.selectByIndex(3);
	
	}
	
	public static void clickContinue() throws InterruptedException{
		WebElement cont1=d1.findElement(By.name("findFlights"));
		Actions a1=new Actions(d1);
		a1.moveToElement(cont1).click().perform();
	}
	
	
	public static void appLaunchPhoto() throws InterruptedException{
		Thread.sleep(3000);
		d1.get("https://upload.photobox.com/en/#computer");
		
	}
	
	public static void uploadPhoto() throws InterruptedException, IOException{
		//Thread.sleep(1000);
		WebElement pht= d1.findElement(By.id("button_desktop"));
		pht.click();
		//Runtime.getRuntime().exec("D:\\Selenium Testing\\photoupload.exe");
		
	}
	
	// *** Sikuli *** 
	public static void sikuliLogin() throws InterruptedException, IOException, FindFailed{
		WebElement uname= d1.findElement(By.name("userName"));
		uname.sendKeys("dasd");
		Thread.sleep(3000);
		WebElement passw = d1.findElement(By.name("password"));
		passw.sendKeys("dasd");
		Thread.sleep(3000);
		Screen sc1=new Screen();
		Pattern p1= new Pattern ("D:\\SeleniumTest\\sikuli_login.png");
		sc1.click(p1);
		
	}
	
	public static void browserClose() throws InterruptedException{
		Thread.sleep(5000);
		d1.close();
	}
	
		
}
