package com.mercury.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

public class Driver {

	public static void main(String[] args) throws InterruptedException, AWTException, IOException, FindFailed {
		// TODO Auto-generated method stub
		
		//MethodRepository m = new MethodRepository();
		MethodRepository.browserLaunch();
	
		MethodRepository.appLaunch();
		//MethodRepository.validLogin();
		//MethodRepository.verifyValidLogin();
		//MethodRepository.departingFromSelection();
		//MethodRepository.clickContinue();
		//MethodRepository.browserClose();
		
		// *** photo upload ***
		//MethodRepository.appLaunchPhoto();
		//MethodRepository.uploadPhoto();
		
		// ***Sikuli***
		MethodRepository.sikuliLogin();
		

	}

}
